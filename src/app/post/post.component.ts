import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {isNumber} from 'util';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postCreated: Date;
  @Output() postLoveItEvent: EventEmitter<number> = new EventEmitter<number>();
  @Input() postLoveIt: number;

  constructor() { }

  ngOnInit() {
  }

  loveIt() {
    console.log('Add love');
    this.postLoveIt ++;
    this.postLoveItEvent.emit(this.postLoveIt);
  }

  dontLoveIt() {
    console.log('Remove love');
    this.postLoveIt --;
    this.postLoveItEvent.emit(this.postLoveIt);
  }

}
