import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  lastUpdate = new Date();
  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLoveIt: number;
  @Input() postCreated: Date;

  constructor() { }

  ngOnInit() {
  }

  countChange(event)  {
    console.log(event);
    this.postLoveIt = event;
  }

}
