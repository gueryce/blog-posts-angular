import { Component } from '@angular/core';
import {isDate, isNumber} from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog-posts-angular';

  posts = [
    {
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in euismod justo.',
      title: 'Mon premier post',
      loveIts: 0,
      created_at : new Date('2008/05/10 12:08:20')
    },
    {
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in euismod justo.',
      title: 'Mon deuxième post',
      loveIts: 0,
      created_at : new Date('2008/05/11 12:08:20')
    },
    {
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in euismod justo.',
      title: 'Encore un post',
      loveIts: 0,
      created_at : new Date('2008/05/12 12:08:20')
    }
  ];

}

